using CSharpFunctionalExtensions;
using Primitives;
using static Primitives.GeneralErrors;

namespace DeliveryApp.Core.Domain.SharedKernel;

/// <summary>
/// Вес в кг.
/// </summary>
public class Weight : ValueObject
{
    /// <summary>
    /// Величина веса, кг.
    /// </summary>
    public int Value { get; }

    private Weight()
    {
    }

    private Weight(int value) : this()
    {
        Value = value;
    }

    /// <summary>
    /// Фабричный метод <see cref="Weight"/>
    /// </summary>
    /// <param name="value">Вес, кг</param>
    /// <returns>Результат</returns>
    public static Result<Weight, Error> Create(int value)
    {
        if (value <= 0) return ValueIsRequired(nameof(value));
        return new Weight(value);
    }

    /// <summary>
    /// Сравнить два веса
    /// </summary>
    /// <param name="first">Вес 1</param>
    /// <param name="second">Вес 2</param>
    /// <returns>Результат</returns>
    public static bool operator <(Weight first, Weight second)
    {
        return first.Value < second.Value;
    }

    /// <summary>
    /// Сравнить два веса
    /// </summary>
    /// <param name="first">Вес 1</param>
    /// <param name="second">Вес 2</param>
    /// <returns>Результат</returns>
    public static bool operator >(Weight first, Weight second)
    {
        return first.Value > second.Value;
    }
        
    /// <summary>
    /// Сравнить два веса
    /// </summary>
    /// <param name="first">Вес 1</param>
    /// <param name="second">Вес 2</param>
    /// <returns>Результат</returns>
    public static bool operator >=(Weight first, Weight second)
    {
        return first.Value >= second.Value;
    }

    /// <summary>
    /// Сравнить два веса
    /// </summary>
    /// <param name="first">Вес 1</param>
    /// <param name="second">Вес 2</param>
    /// <returns>Результат</returns>
    public static bool operator <=(Weight first, Weight second)
    {
        return first.Value <= second.Value;
    }
    
    /// <inheritdoc cref="GetEqualityComponents"/>
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
}