using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.SharedKernel;

/// <summary>
/// Координата.
/// </summary>
public class Location : ValueObject
{
    /// <summary>
    /// Максимальная координата.
    /// </summary>
    public static readonly Location Min = new(1, 1);

    /// <summary>
    /// Минимальная координата
    /// </summary>
    public static readonly Location Max = new(10, 10);
    
    /// <summary>
    ///  Координата Х.
    /// </summary>
    public int X { get; }
    
    /// <summary>
    /// Координата Y.
    /// </summary>
    public int Y { get; }
    
    private Location(){}
    
    private Location(int x, int y):this()
    {
        X = x;
        Y = y;
    }

    /// <summary>
    /// Фабричный метод <see cref="Location"/>
    /// </summary>
    /// <param name="x">Координата X.</param>
    /// <param name="y">Координата Y.</param>
    /// <returns>Результат.</returns>
    public static Result<Location, Error> Create(int x, int y)
    {
        if (x < Min.X || x > Max.X) return GeneralErrors.ValueIsInvalid(nameof(x));
        if (y < Min.Y || y > Max.Y) return GeneralErrors.ValueIsInvalid(nameof(y));

        return new Location(x, y);
    }
    
    /// <inheritdoc cref="GetEqualityComponents"/>
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return X;
        yield return Y;
    }

    /// <summary>
    /// Расстояние до точки.
    /// </summary>
    /// <param name="targetLocation">Конечная точка</param>
    /// <returns>Результат.</returns>
    public Result<int, Error>  DistanceTo(Location targetLocation)
    {
        var xLen = Math.Abs(targetLocation.X - X);
        var yLen = Math.Abs(targetLocation.Y - Y);
        return xLen + yLen;
    }
}