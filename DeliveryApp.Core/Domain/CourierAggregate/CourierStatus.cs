using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate;

/// <summary>
/// Статус курьера.
/// </summary>
public class CourierStatus : ValueObject
{
    /// <summary>
    /// Курьер недоступен.
    /// </summary>
    public static CourierStatus NotAvailable => Create(nameof(NotAvailable).ToLowerInvariant()).Value;
    
    /// <summary>
    /// Курьер готов.
    /// </summary>
    public static CourierStatus Ready => Create(nameof(Ready).ToLowerInvariant()).Value;
    
    /// <summary>
    /// Курьер занят.
    /// </summary>
    public static CourierStatus Busy => Create(nameof(Busy).ToLowerInvariant()).Value;
    
    /// <summary>
    /// Статус.
    /// </summary>
    public string Value { get; }

    private CourierStatus()
    { }

    private CourierStatus(string value):this()
    {
        Value = value;
    }

    /// <summary>
    /// Фабричный метод <see cref="CourierStatus"/>
    /// </summary>
    /// <param name="value">Статус</param>
    /// <returns>Результат.</returns>
    private static Result<CourierStatus, Error> Create(string value)
    {
        if (string.IsNullOrEmpty(value))
        {
            return GeneralErrors.ValueIsRequired(nameof(value));
        }

        return new CourierStatus(value);
    }

    /// <summary>
    /// Получить статус курьера по названию.
    /// </summary>
    /// <param name="name">Название статуса.</param>
    /// <returns>Резултат: статус курьера.</returns>
    public static Result<CourierStatus, Error> FromName(string name)
    {
        var status = List().FirstOrDefault(x =>
            string.Equals(x.Value, name, StringComparison.InvariantCultureIgnoreCase));
        if (status == null)
        {
            return GeneralErrors.ValueIsInvalid(nameof(name));
        }

        return status;
    }
    
    /// <summary>
    /// Список всех значений списка
    /// </summary>
    /// <returns>Значения списка</returns>
    public static IEnumerable<CourierStatus> List()
    {
        yield return NotAvailable;
        yield return Ready;
        yield return Busy;
    }

    /// <inheritdoc cref="GetEqualityComponents"/>>
    protected override IEnumerable<IComparable> GetEqualityComponents()
    {
        yield return Value;
    }
}