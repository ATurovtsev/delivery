using System.Diagnostics.CodeAnalysis;
using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.OrderAggregate;

public class OrderStatus : ValueObject
{
    public static OrderStatus Created => Create(nameof(Created).ToLowerInvariant()).Value;
        public static OrderStatus Assigned => Create(nameof(Assigned).ToLowerInvariant()).Value;
        public static OrderStatus Completed => Create(nameof(Completed).ToLowerInvariant()).Value;

        /// <summary>
        /// Значение.
        /// </summary>
        public string Value { get; }

        private OrderStatus()
        { }

        private OrderStatus(string input)
        {
            Value = input;
        }

        /// <summary>
        ///  Фабричный метод <see cref="OrderStatus"/>
        /// </summary>
        /// <returns>Результат</returns>
        private static Result<OrderStatus, Error> Create(string input)
        {
            return string.IsNullOrWhiteSpace(input) ? GeneralErrors.ValueIsRequired(nameof(input)) : new OrderStatus(input);
        }

        /// <summary>
        /// Получить статус по названию.
        /// </summary>
        /// <param name="name">Название.</param>
        /// <returns>Статус</returns>
        public static Result<OrderStatus, Error> FromName(string name)
        {
            var status = List()
                .SingleOrDefault(s => string.Equals(s.Value, name, StringComparison.CurrentCultureIgnoreCase));
            if (status == null)
            {
                return GeneralErrors.ValueIsInvalid(nameof(name));
            }
            return status;
        }

        /// <summary>
        /// Список всех значений списка.
        /// </summary>
        /// <returns>Значения списка.</returns>
        public static IEnumerable<OrderStatus> List()
        {
            yield return Created;
            yield return Assigned;
            yield return Completed;
        }

        /// <inheritdoc cref="GetEqualityComponents"/>
        protected override IEnumerable<IComparable> GetEqualityComponents()
        {
            yield return Value;
        }

}