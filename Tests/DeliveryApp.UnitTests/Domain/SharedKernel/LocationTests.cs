using System.Threading.Tasks;
using DeliveryApp.Core.Domain.SharedKernel;
using FluentAssertions;
using Xunit;

namespace DeliveryApp.UnitTests.Domain.SharedKernel;

public class LocationTests
{
    [Theory]
    [InlineData(3, 3)]
    [InlineData(1, 2)]
    [InlineData(3, 10)]
    public void ValidCoordsLocation_Created_Success(int x, int y)
    {
        var location = Location.Create(x, y);

        location.IsSuccess.Should().BeTrue();
        location.Value.X.Should().Be(x);
        location.Value.Y.Should().Be(y);
    }
    
    [Theory]
    [InlineData(0, 0)]
    [InlineData(11, 2)]
    [InlineData(-3, -10)]
    public void InvalidCoordsLocation_Creation_Error(int x, int y)
    {
        var location = Location.Create(x, y);

        location.IsSuccess.Should().BeFalse();
        location.Error.Should().NotBeNull();
    }
}